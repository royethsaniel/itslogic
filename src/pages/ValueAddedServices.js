import React, { Fragment, useEffect } from 'react';
import MetaTags from 'react-meta-tags';

import Loading from '../blocks/loading/Loading';
import Header from '../blocks/header/Header';
import Footer from '../blocks/footer/Footer';

import PageTitleCommon from '../blocks/page-title/PageTitleCommon';
import ServiceInsideSwiper from '../blocks/service-inside/ServiceInsideSwiper';
import ValueAddedServiceSwiper from '../blocks/service-inside/ValueAddedServiceSwiper';
import ServiceInsideWidget from '../blocks/service-inside/ServiceInsideWidget';
import valueAddedServicesBg from '../assets/img/bg/service-valueAddedServices.jpg';

const ValueAddedServices = () => {
    useEffect( () => {
        document.body.classList.add( 'single' );
        document.body.classList.add( 'single-adveits_service' );

        return () => {
            document.body.classList.remove( 'single' );
            document.body.classList.remove( 'single-adveits_service' );
        }
    }, [] );

    return (
        <Fragment>
            <MetaTags>
                <meta charSet="UTF-8" />
                <title>Value Added Services | ITSLOGIC Freight Forwarding</title>

                <meta httpEquiv="x-ua-compatible" content="ie=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta name="description" content="" />
                <meta name="keywords" content="" />
                <meta name="robots" content="index, follow, noodp" />
                <meta name="googlebot" content="index, follow" />
                <meta name="google" content="notranslate" />
                <meta name="format-detection" content="telephone=no" />
            </MetaTags>

            <Loading />

            <Header logoColor="dark" />

            <main id="main" className="site-main">
                <PageTitleCommon bg={valueAddedServicesBg} title="Value Added Service" />

                <section id="page-content" className="spacer p-top-xl">
                    <div className="wrapper">
                        <div className="content">
                            <div id="single">
                                <div className="row gutter-width-md single-content">
                                    <div className="col-xl-8 col-lg-8 col-md-12 col-sm-12">
                                        <ValueAddedServiceSwiper/>
                                    </div>

                                    <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                                        <aside id="aside" className="widget-area style-default">
                                            <div id="service" className="widget_service">
                                                {/* <ServiceInsideWidget /> */}
                                                <div className="description style clearfix spacer m-top-lg">
                                            <ul>
                                                <li>
                                                    <h5>Customs Brokerage</h5>
                                                </li>
                                                <li>
                                                    <h5>Warehousing</h5>
                                                </li>
                                                <li>
                                                    <h5>Packaging Services (Co-packing, shrinking, wrapping)</h5>
                                                </li>
                                                <li>
                                                    <h5>Trucking</h5>
                                                </li>
                                            </ul>
                                        </div>
                                            </div>
                                        </aside>
                                    </div>
                                     
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>

            <Footer />
        </Fragment>
    );
};

export default ValueAddedServices;
