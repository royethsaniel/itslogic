import React from 'react';

import GoogleMaps from '../google-maps/GoogleMaps';
import ContactsAccordion from './ContactsAccordion';

const ContactsContent = () => {
    return (
        <div id="maps" className="maps style-default">
            {/* <GoogleMaps /> */}
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3863.332269784334!2d120.97223158472663!3d14.465600094725744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397cde9a5555555%3A0x101f16a46a003384!2sBernice%20Townhouse%20At%20Las%20Pinas%20City!5e0!3m2!1sen!2sph!4v1617343566544!5m2!1sen!2sph" width="2000" height="600" style={{border:0}} allowFullScreen="" loading="lazy"></iframe>
            <ContactsAccordion />
        </div>
    );
};

export default ContactsContent;
