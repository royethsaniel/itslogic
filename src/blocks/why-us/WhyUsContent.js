import React from 'react';

import WhyUsPdf from './WhyUsPdf';

const Commodities = () => {
    return (
        <div className="wrapper">
            <div className="content">
                <div className="clearfix">
                    <div className="row gutter-width-lg with-pb-lg style-default">
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h4 className="text-secondary">01</h4>
                            <p className="text-white p-large bold">MISSION</p>
                            <p className="text-white p-small">To be as competent with major players in 
                            Freight Forwarding and Logistics Industry,
                            offering services and applying the outstanding
                            proceedings in Freight Forwarding and 
                            Logistics trades.</p>
                        </div>

                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h4 className="text-secondary">02</h4>
                            <p className="text-white p-large bold">VISION</p>
                            <p className="text-white p-small">To be clients preference by providing 
                            reliable transport solutions both domestic 
                            and international services.</p>
                        </div>

                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h4 className="text-secondary">03</h4>
                            <p className="text-white p-large bold">CORE VALUES</p>
                            <p className="text-white p-small">The entire team is guided by values of integrity,
                            commitment and excellence. We value and treat
                            everyone as an asset of the company</p>
                        </div>

                        {/* <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h4 className="text-secondary">04</h4>
                            <p className="text-primary p-large bold">Versatility</p>
                            <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms.</p>
                        </div>

                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <h4 className="text-secondary">05</h4>
                            <p className="text-primary p-large bold">Responsibility</p>
                            <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms.</p>
                        </div>

                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <WhyUsPdf />
                        </div> */}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Commodities;
