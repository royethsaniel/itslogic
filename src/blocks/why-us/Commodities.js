import React from 'react';

const items = ['GENERAL CARGOES', 'PERSONAL EFFECTS', 'FOOD PRODUCTS', 'LIVE ANIMALS', 'PROJECT CARGOES', 'DANGEROUS GOODS', 'UNNACOMPANIED BAGGAGE', 'ELECTRONICS', 'ENGINE', 'SPARE PARTS', 'VEHICLES, MOTORCYCLES', 'PERISHABLE', 'HUMAN REMAINS', 'OVERSIZED SHIPMENTS', 'OTHERS']
const Commodities = () => {
    return (
        <div className="wrapper">
            <div className="content">
                <div className="clearfix">
                    <div className="row gutter-width-lg with-pb-lg style-default">
                        <div className="col-12 text-center">
                            <h4 className="text-secondary">OUR COMMODITIES</h4>
                            <p style={{color: '#191919'}}><strong>ITSLOGIC FREIGHT</strong> is well-versed in handling different commodities such as:</p>
                            {items.map((item)=> 
                            <div style={{display: 'inline-block', border: '1.5px solid #383838'}} className="m-2 p-2">
                            <span style={{color: '#9A7F3E ', marginTop: 0}} className="p-small">{item}</span>
                            </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
               
        </div>
    );
}

export default Commodities;
