import React from 'react';

import FooterMenu from '../footer/FooterMenu';

const FooterWidget = () => {
    return (
      <div className="footer-widgets">
          <div className="footer-widget-area d-flex flex-wrap">
              <div className="widget">
                  <h5 className="widget-title">About us</h5>

                  <p className="p-small">ITS LOGIC is currently establishing connections across
                    the globe to boost our capabilities in handling complex 
                    freight forwarding requirements in delivering our
                    commitment worldwide</p>
              </div>

              <div className="widget">
                  <h5 className="widget-title">Menu</h5>

                  <FooterMenu />
              </div>

              <div className="widget">
                  <h5 className="widget-title">Contacts</h5>

                  <p>Block 2 Lot 4 Bernice Townhouse Zapote
                Las Pinas City
                Philippines</p>
                <p>+639 171 729 518</p>
                <p>customerservice@itslogic.ph</p>
{/* 
                  <p><a href="mailto:info@domain.com">info@domain.com</a></p>

                  <p>832-576-5370</p> */}
              </div>
          </div>
      </div>
    );
};

export default FooterWidget;
