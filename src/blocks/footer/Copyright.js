import React from 'react';

const Copyright = () => {
    return (
      <div className="copyright">
          <p>© 2021 ITSLOGIC FREIGHT All Rights Reserved.</p>
      </div>
    );
};

export default Copyright;
