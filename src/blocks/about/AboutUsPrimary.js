import React from 'react';

const LinkTitle = () => {
    return (                    
        <a title="About us" className="transform-scale-h" href={ `${ process.env.PUBLIC_URL + "/about-us" }` }>
            About us<i className="fas fas-space-l fa-long-arrow-alt-right align-top"></i>
        </a>
    );
};
  
const TextTitle = () => {
    return ( <div title="About us">About us</div> );
};

const AboutUsTitle = ( props ) => {

const isHomepage = props.isHomepage;

    if ( isHomepage ) {
        return <LinkTitle />;
    }
        return <TextTitle />;
};

const AboutUsPrimary = ( props ) => {
    return (
        <div className="col-lg-6 col-md-12 col-sm-12 col-12 align-self-center mb-3">
            <div className="title">
                <h2>
                    <AboutUsTitle isHomepage={ props.isHomepage } />
                </h2>
            </div>

            <div className="text">
                <p><strong>INTERNATIONAL TRANSPORT SOLUTIONS LOGISTICS 
                FREIGHT FORWARDING</strong> is a duly registered freight 
                forwarding company in the Philippines established 
                in November 2018. It’s a locally owned company 
                managed by a group of individuals with wide 
                years of experience in cargo airlines and freight 
                forwarding industries. 
                <br/> <br/>
                We were able to build the trust and confidence of our 
                clients and business partners.</p>
            </div>

            {/* <div className="list-group list-group-horizontal spacer m-top-lg m-bottom-lg-media-xl style-default">
                <div className="list-group-item">
                    <h4 className="text-secondary">03</h4>
                    <p>Countries</p>
                </div>

                <div className="list-group-item">
                    <h4 className="text-secondary">25</h4>
                    <p>Offices</p>
                </div>

                <div className="list-group-item">
                    <h4 className="text-secondary">154</h4>
                    <p>Employees</p>
                </div>
            </div> */}
        </div>
    );
};

export default AboutUsPrimary;
